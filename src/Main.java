import javafx.application.Application;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Main extends Application {

    int sceneWidth = 600;
    int sceneHeight = 500;
    int tableWidth = 401;
    int columnWidth = tableWidth/4;
    int tableHeight = sceneHeight-150;

    TextField nameField = new TextField();
    TextField taskField = new TextField();
    TextField hourField = new TextField();
    TextField minuteField = new TextField();
    Button button = new Button("Enter reg");
    TableView<TimeRegistration> tableView = new TableView<>();
    ObservableList<TimeRegistration> registrationList = FXCollections.observableArrayList();


    @Override
    public void start(Stage stage) throws Exception {
        nameField = new TextField();
        taskField = new TextField();
        hourField = new TextField();
        minuteField = new TextField();
        button = new Button("Enter reg");
        tableView = new TableView<>();
        tableView.setPrefWidth(tableWidth);
        tableView.setPrefHeight(tableHeight);
        registrationList = FXCollections.observableArrayList();

        Group root = new Group();
        Scene scene = new Scene(root, sceneWidth, sceneHeight);


        //Create tex areas and 1 button
        nameField.setLayoutX(410);
        nameField.setLayoutY(100);

        taskField.setLayoutX(410);
        taskField.setLayoutY(150);

        hourField.setLayoutX(410);
        hourField.setLayoutY(200);

        minuteField.setLayoutX(410);
        minuteField.setLayoutY(250);

        button.setLayoutX(410);
        button.setLayoutY(300);

        nameField.setPromptText("Name");
        taskField.setPromptText("Task");
        hourField.setPromptText("Hour");
        minuteField.setPromptText("Minute");

        root.getChildren().addAll(nameField, taskField, hourField, minuteField, button);


        //Create table columns
        TableColumn<TimeRegistration, String> registrationName = new TableColumn<>("Name");
        TableColumn<TimeRegistration, String> registrationTask = new TableColumn<>("Task");
        TableColumn<TimeRegistration, Integer> registrationHour = new TableColumn<>("Hour");
        TableColumn<TimeRegistration, Integer> registrationMinute = new TableColumn<>("Minute");
        TableColumn<TimeRegistration, Integer> nestedColumn = new TableColumn<>("Time");
        registrationName.setPrefWidth(columnWidth);
        registrationTask.setPrefWidth(columnWidth);
        registrationHour.setPrefWidth(columnWidth);
        registrationMinute.setPrefWidth(columnWidth);
        nestedColumn.setPrefWidth(columnWidth*2);
        nestedColumn.getColumns().addAll(registrationHour, registrationMinute);
        //Add all columns to the tableview
        tableView.getColumns().addAll(registrationName, registrationTask, nestedColumn);
        //add the tableview to the scene
        root.getChildren().add(tableView);

        //Select what the columns should display
        registrationName.setCellValueFactory(x -> x.getValue().nameProperty());
        registrationTask.setCellValueFactory(x -> x.getValue().taskProperty());
        registrationHour.setCellValueFactory(x -> x.getValue().hourProperty());
        registrationMinute.setCellValueFactory(x -> x.getValue().minuteProperty());


        registrationList.add(new TimeRegistration("name1", "task1", 13, 30));
        registrationList.add(new TimeRegistration("name2", "task2", 14, 00));
        tableView.setItems(registrationList);

        stage.setScene(scene);
        stage.show();
        stage.setTitle("Time registration");

        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                addToList();
                nameField.clear();
                taskField.clear();
                hourField.clear();
                minuteField.clear();
            }
        });


       registrationList.addListener(new ListChangeListener<TimeRegistration>() {
           @Override
           public void onChanged(ListChangeListener.Change change) {
               updateTable();
           }
       });



    }

    public void addToList(){
        try{
            if(nameField.getText().length() == 0 || taskField.getText().length() == 0){
                System.out.println("empty fields");
                return;
            }
            TimeRegistration newEntry = new TimeRegistration(nameField.getText(), taskField.getText(), Integer.parseInt(hourField.getText().trim()), Integer.parseInt(minuteField.getText().trim()));
            registrationList.add(newEntry);
        }catch (Exception e){
            System.out.println("Error");
        }
    }

    public void updateTable(){
        tableView.setItems(registrationList);
    }
}
