import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;

public class TimeRegistration {
    SimpleStringProperty name;
    SimpleStringProperty task;

    ObservableValue<Integer> hour;
    ObservableValue<Integer> minute;

    public TimeRegistration(String name, String task, int hour, int minute){
        this.name = new SimpleStringProperty(name);
        this.task = new SimpleStringProperty(task);
        this.hour = new SimpleIntegerProperty(hour).asObject();
        this.minute = new SimpleIntegerProperty(minute).asObject();
    }



    public SimpleStringProperty nameProperty() {
        return name;
    }


    public SimpleStringProperty taskProperty() {
        return task;
    }


    public ObservableValue<Integer> hourProperty() {
        return hour;
    }


    public ObservableValue<Integer> minuteProperty() {
        return minute;
    }
}
